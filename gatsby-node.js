/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require(`path`)
const fs = require("fs")

const LANGS = ["en", "es"]
const LANGUAGES = ["english", "spanish"]

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions
  const pageTemplate = path.resolve(`src/templates/pageTemplate.js`)
  const postTemplate = path.resolve(`src/templates/postTemplate.js`)
  const AdminPage = path.resolve(`src/templates/adminPage.js`)
  const activitiesPageTemplate = path.resolve(
    `src/templates/activities-page.js`
  )
  const documentsPageTemplate = path.resolve(`src/templates/documents-page.js`)

  const pagesResult = await graphql(`
    {
      allMarkdownRemark(filter: { frontmatter: { type: { eq: "page" } } }) {
        nodes {
          id
          frontmatter {
            title
            path
            order
            type
            lang
            path_en
            path_es
          }
          htmlAst
        }
      }
    }
  `)
  // Handle errors
  if (pagesResult.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  pagesResult.data.allMarkdownRemark.nodes
    .filter(node => {
      return node.frontmatter.type === "page"
    })
    .forEach(node => {
      if (!node.frontmatter.path) {
        console.warn("Frontmatter has no path", node.frontmatter)
        throw new Error("Page must have path")
      }
      createPage({
        path: node.frontmatter.path,
        component: pageTemplate,
        context: {
          data: node,
        }, // additional data can be passed via context
      })
    })

  const activitiesResult = await graphql(`
    query ActivityPageContent {
      allMarkdownRemark(filter: { frontmatter: { type: { eq: "activity" } } }) {
        nodes {
          id
          html
          frontmatter {
            lang
            date
            title
            imageGallery
            imageGalleryDirection
            videos {
              url
              title
              coverImage {
                uid
                absolutePath
                childImageSharp {
                  fixed {
                    src
                    srcSet
                    tracedSVG
                    width
                    height
                    aspectRatio
                  }
                  fluid {
                    srcSet
                    presentationWidth
                    presentationHeight
                  }
                }
              }
            }
          }
        }
      }
    }
  `)
  if (activitiesResult.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const activitiesPerLanguage = {}
  activitiesResult.data.allMarkdownRemark.nodes.forEach(async node => {
    const language = node.frontmatter.lang

    if (node.frontmatter.imageGallery) {
      const imagesQueryResult = await graphql(`
      query {
        allFile(filter: { relativeDirectory: { eq: "${node.frontmatter.imageGallery}" } }) {
          totalCount
          edges {
            node {
              id
              childImageSharp {
                id
                fixed {
                  src
                  srcSet
                  tracedSVG
                  width
                  height
                  aspectRatio
                }
                fluid {
                  src
                  presentationWidth
                  presentationHeight
                  aspectRatio
                }
              }
            }
          }
        }
      }
      `)
      if (imagesQueryResult.errors) {
        reporter.panicOnBuild(`Error while querying activity images`)
        return
      }
      node.galleryImages = imagesQueryResult.data.allFile.edges.map(edge => {
        return edge.node.childImageSharp
      })
    }

    if (!activitiesPerLanguage[language]) {
      activitiesPerLanguage[language] = []
    }
    activitiesPerLanguage[language].push({
      date: node.frontmatter.date,
      ...node,
    })
  })

  const wpActivities = await graphql(`
    {
      allWordpressPost(
        filter: { categories: { elemMatch: { name: { eq: "activity" } } } }
      ) {
        nodes {
          id
          title
          excerpt
          content
          status
          tags {
            name
          }
          date
        }
      }
    }
  `)

  wpActivities.data.allWordpressPost.nodes.forEach(wpActivityNode => {
    const languageTag = wpActivityNode.tags.find(tag =>
      LANGUAGES.includes(tag.name)
    )
    if (!languageTag) {
      console.warn(
        `Post ${wpActivityNode.title} has no language tag set, skipping`
      )
      return
    }

    const dateTag = wpActivityNode.tags.find(tag =>
      /^\d{4}-\d{2}(-\d+)?$/.test(tag.name)
    )
    if (!dateTag) {
      console.warn("WP Activity has no date tag, skipping")
      return
    }
    const language = languageTag.name
    const nodeWithType = {
      type: "wordpress_activity",
      date: dateTag.name,
      ...wpActivityNode,
    }
    if (language === "english") {
      activitiesPerLanguage.en.push(nodeWithType)
    } else if (language === "spanish") {
      activitiesPerLanguage.es.push(nodeWithType)
    } else {
      console.warn("Language not set for item", wpActivityNode.title)
    }
  })

  if (wpActivities.errors) {
    reporter.panicOnBuild(`Error while querying activities from wordpress`)
    return
  }

  createPage({
    path: "/activities",
    component: activitiesPageTemplate,
    context: {
      data: activitiesPerLanguage.en,
      lang: "en",
    },
  })
  createPage({
    path: "/actividades",
    component: activitiesPageTemplate,
    context: {
      data: activitiesPerLanguage.es,
      lang: "es",
    },
  })

  const blogResult = await graphql(
    `
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { name: { eq: "blog" } } } }
        ) {
          totalCount
          nodes {
            content
            title
            slug
            excerpt
            tags {
              name
            }
            date
          }
        }
      }
    `
  )

  blogResult.data.allWordpressPost.nodes.forEach(newsNode => {
    const path = `/${newsNode.slug}`

    createPage({
      path,
      component: postTemplate,
      context: {
        data: newsNode,
      },
    })
  })

  const documentsResult = await graphql(`
    query {
      allWordpressPost(
        filter: {
          categories: {
            elemMatch: { name: { in: ["documentos", "documents"] } }
          }
        }
      ) {
        nodes {
          id
          title
          content
          excerpt
          slug
          tags {
            name
          }
          date
        }
      }
    }
  `)
  const documentNodes = {
    en: [],
    es: [],
  }

  const documentsDataNodes = documentsResult.data
    ? documentsResult.data.allWordpressPost.nodes
    : []
  const DateTagRegex =  /\d{4}-\d{2}(-d+)?/
  documentsDataNodes.sort((n1, n2) => {
    const n1DateTag = n1.tags.find(tag => DateTagRegex.test(tag.name))
    const n2DateTag = n2.tags.find(tag => DateTagRegex.test(tag.name))
    if (n1DateTag && n2DateTag) {
      return n1DateTag.name.localeCompare(n2DateTag.name)
    } else {
      return 0
    }
  })

  // show most recent documents fist
  documentsDataNodes.reverse()

  documentsDataNodes.forEach(documentDataNode => {
    const tagNames = documentDataNode.tags.map(tag => tag.name)
    console.log(documentDataNode.id, tagNames)
    if (tagNames.includes("english")) {
      documentDataNode.lang = "en"
      documentNodes.en.push(documentDataNode)
    } else if (tagNames.includes("spanish")) {
      documentDataNode.lang = "es"
      documentNodes.es.push(documentDataNode)
    } else {
      documentNodes.en.push({ ...documentDataNode, lang: "en" })
      documentNodes.es.push({ documentDataNode, lang: "es" })
    }
  })

  const DOCUMENTS_PATH = {
    en: "documents",
    es: "documentos",
  }
  const linkForDocumentPage = (node, lang) => {
    return `/${DOCUMENTS_PATH[lang]}/${lang}/${node.slug}`
  }

  createPage({
    path: `/${DOCUMENTS_PATH.en}`,
    component: documentsPageTemplate,
    context: {
      data: documentNodes.en,
      lang: "en",
    },
  })
  createPage({
    path: `/${DOCUMENTS_PATH.es}`,
    component: documentsPageTemplate,
    context: {
      data: documentNodes.es,
      lang: "es",
    },
  })

  LANGS.forEach(lang => {
    documentNodes[lang].forEach(documentNode => {
      const language = documentNode.lang
      createPage({
        path: linkForDocumentPage(documentNode, language),
        component: pageTemplate,
        context: {
          data: {
            ...documentNode,
          },
        },
      })
    })
  })
  if (process.env.ENV === "staging") {
    createPage({
      path: "/admin",
      component: AdminPage,
    })
  }
}

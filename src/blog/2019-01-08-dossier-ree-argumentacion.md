---
type: "post"
date: 2019-10-08
lang: "es"
---
# Descripción de los proyectos de REE en las provincias de Granada y Almería

En este documento trataremos de sintetizar, en la medida de lo posible, la situación actual y los proyectos a corto plazo de Red Electrica en el tendido eléctrico que une las provincias de Almería y Granada. Para empezar nos centraremos en describir brevemente la infraestructura actual, para continuar con cada proyecto, sus detalles técnicos básicos, justificación y mapas.

1. Infraestructuras actuales
1. Proyecciones
    1. L/400 kV DC Baza-Caparacena
    1. TS-1: Duplicación del eje de 220 kV
    1. Aumentos de capacidad de transporte
3. Comparativa actual-final
4. Débiles argumentaciones
5. La “transparencia” de REE
* Anexo: Características técnicas de las actuaciones

## Infraestructuras actuales

Nos basaremos en el mapa proporcionado por la Consejería de Empleo, Empresa y Comercio de la Junta de Andalucía en su justificación del Eje de Transporte Eléctrico Caparacena-Baza-La Ribina 400 kV

![](./images/dossier-image-1.png)

En este podemos observar las dos líneas de AT que conectan a día de hoy Sur con Levante. Por la cara norte de Sierra Nevada encontramos la línea L/400 kV Caparacena-Hueneja-Tabernas-Litoral, con una Capacidad de Transporte de 1.400 MVA, y entre la cara sur y Sierra Lujar encontramos la línea L/220 kV Caparacena-Gabias-Órgiva-Benahadux-Tabernas, de 334 MVA.
Es importante dejar claro que nos estamos centrando en las líneas de transporte (≥220 kV) y no de distribución (<220 kV). Estas se destinan al transporte de la energía a largas distancias y no al suministro local.

## Proyecciones

A continuación, exponemos los proyectos que se quieren llevar a cabo en esta región basándonos en dos documentos oficiales del Ministerio de Industria, Energía y Turismo: el Plan de Desarrollo de la Red de Transporte de Energía Eléctrica 2008-2016 y el 2015-2020.

### L/400 kV DC Baza-Caparacena

La línea eléctrica de 400 kV Baza-Caparacena forma parte del nuevo eje de doble circuito (DC) Ribina-Baza-Caparacena que tiene como objetivo además del mallado de la Red de Transporte, facilitar la evacuación de la generación de renovables prevista en la región y por último, el apoyo a la red de distribución con objeto de garantizar la alimentación de la elevada demanda prevista en la zona de Andalucía Oriental. Su capacidad de transporte será de 2.441 MVA/circuito (total: 4.882 MVA). Aquí tenemos el mapa del recorrido que seguiría la línea:

![](./images/dossier-image-2.1.png)

Esta línea se proclama como proyecto a favor del desarrollo energético y socioeconómico de la región de Andalucía Oriental. Ha provocado gran discordia entre PSOE y PP, ya que en 2008, con Zapatero (PSOE) en el gobierno, apareció por primera vez en el Plan de Desarrollo de la Red de Transporte de Energía Eléctrica 2008-2016 como una de las líneas prioritarias. Sin embargo, en Diciembre de 2011, Rajoy (PP) toma posesión del nuevo gobierno y en el siguiente Plan de Desarrollo de la Red de Transporte de Energía Eléctrica 2015-2020 encontramos este proyecto en el Anexo 2, referente a las actuaciones menos prioritarias y con horizonte de ejecución siempre posterior a 2020 (sin precisar cuándo), lo que generó un largo periodo de reproches al respecto entre ambos partidos.
Finalmente, la llegada al gobierno de Pedro Sánchez, ha permitido en apenas unas semanas que se haya aprobado en Acuerdo del Consejo de Ministros y Ministras de 27 de Julio de 2018 la modificación de aspectos puntuales del documento planificación energética. Esta modificación publicada en la página web del nuevo Ministerio para la Transición Ecológica, traslada la línea al Anexo 1, y por tanto, la incluye entre las prioritarias, con un horizonte de ejecución determinado (2020) y una financiación suficiente.
Parece que resulta bastante fácil modificar las actuaciones de REE cuando hay verdadera intención política. Las obras empezarán este mismo Otoño de 2019 y terminarán antes de 2022.

### TS-1: Duplicación del eje de 220 kV entre Granada y Almería

El proyecto consiste en duplicar el eje de 220 kV existente entre Granada y Almería, con objeto de fortalecer la interconexión Sur-Levante, ya que según REE, actualmente la única interconexión es el eje de 400 kV Caparacena-Huéneja-Tabernas-Litoral. Esto provocaría que en escenarios de alta importación desde Levante hacia el Sur, unido a una elevada producción de régimen especial y al fallo del eje de 400 kV, se produjeran sobrecargas máximas del 21% en la línea L/220 kV Caparacena-Gabias-Órgiva-Benahadux-Tabernas. Resulta incoherente hablar de una única interconexión Sur-Levante cuando posteriormente se habla de que ante el fallo de esta “única interconexión” la otra se sobrecarga. A continuación, hablaremos de las infraestructuras que forman parte de este proyecto dividido en varias actuaciones:

* Nueva subestación Saleres 220 kV
    * SE Saleres 220 kV
    * L/220 kV DC E/S Saleres – L/Gabias-Órgiva [711-861 MVA/circuito]
    * L/220 kV DC E/S Saleres – L/Berja-Órgiva [711,7-862,5 MVA/circuito]
* L/220 kV SC El Fargue-Saleres [712,7-863,7 MVA/circuito]
* L/220 kV (aislada a 400 kV) DC Benahadux-Saleres [800-970 MVA/circuito]

![](./images/dossier-image-2.2.png)

En el Estudio de Impacto Ambiental L/220 KV Albuñuelas – Benahadux, previo al actual Estudio de Impacto Ambiental L/220 KV Benahadux – Saleres, encontramos una frase de gran relevancia, que en el informe más reciente desaparece. La frase afirma que “la infraestructura creada permitirá obtener importantes beneficios al conjunto del sistema, por facilitar el mejor aprovechamiento de los recursos del mismo, sumando robustez al conjunto, aumentando la fiabilidad y reduciéndose con ello la necesidad de nuevos equipamientos”. Se ha prescindido de esta afirmación en los últimos informes, quizá como medida de prevención por parte de REE en caso de querer justificar nuevas actuaciones en la región. Este proyecto es el principal causante de la movilización ciudadana en la Alpujarra y el Valle de Lecrín.

### Aumentos de capacidad de transporte

Las actuaciones expuestas a continuación se encuentran recogidas en la Planificación Energética 2015-2020. Las trataremos conjuntamente ya que ambas actuaciones son muy similares y se justifican bajo los mismos criterios. Hablamos de la repotenciación de los dos ejes existentes que inicialmente hemos descrito, tanto el de 400 kV, que pasaría de los 1.398 MVA actuales a 1.829 MVA, como el de 220 kV, pasando de 334 MVA a 439 MVA. Para lograr este aumento de capacidad se incrementará la máxima de temperatura de operación de 50°C a 85°C, lo que implica una mayor dilatación de los conductores, resultando que en determinados tramos no se cumplen las distancias mínimas reglamentarias entre cableado y suelo. En este sentido, la solución adoptada consiste en el recrecido de los apoyos que forman parte de dichos tramos.
Por otro lado, de nuevo se menciona que en la zona oriental de Andalucía, en escenarios de alta importación desde Levante hacia el Sur y unido a una elevada producción de generación de origen renovable en la zona, en situaciones de contingencias en la red de 400 kV (Tajo de la Encantada- Caparacena-Huéneja-Tabernas-Litoral) se producen sobrecargas en las líneas de 220 kV (Caparacena-Atarfe-Órgiva-Benahadux), que transcurren en paralelo con el eje de 400 kV. En ambos casos se nos habla de una actuación de carácter estructural, ya que contribuirá al buen funcionamiento del sistema eléctrico en su conjunto a nivel zonal. Concretamente van dirigidas a reforzar el mallado eléctrico de la región.

# Comparativa actual-final

Tras haber descrito cada actuación y haber leído su justificación pasaremos a analizar la situación en su conjunto. Para empezar compararemos la situación inicial con la hipotética situación final para hacernos a la idea de la transformación que se daría en la región.

![](./images/dossier-image-3a.png)

En este mapa de la situación inicial vemos que la capacidad de transporte entre Granada y Almería hoy en dia es de 1.732 MVA. Teniendo en cuenta que una de las justificaciones más utilizadas en las actuaciones es la de aumentar la capacidad de transporte Sur-Levante, vamos a compararla con la que tendríamos en el caso hipotético de que todas las actuaciones planificadas se llevaran a cabo.

![](./images/dossier-image-3b.png)

Aquí ya podemos ver como lo que antes eran dos ejes paralelos, uno de 400 kV y otro de 220 kV, pasarían a ser dos ejes de 400 kV en la parte norte de Sierra Nevada, y dos ejes de 220 kV en la parte sur, con la previsión de que la línea Benahadux-Saleres pase a ser de 400 kV en algún momento. La capacidad de transporte en este caso sería de 9.090 MVA, lo que supondría un incremento del 524,8 % en la capacidad de transporte. Nos parece una exageración, y más teniendo en cuenta el informe del estadounidense Electric Power Research Institute que afirma que, en 2010, REE ya había sobredimensionado en un 198% la red de 400 kV, doblando el sistema eléctrico de la mayor parte de países del resto de Europa. Y la idea de REE es seguir aumentando este sistema. Esta línea ya ha recibido el apoyo de ayuntamientos de toda la región y ya ha recibido el derecho de interés público.
Por otro lado, la actuación “TS-1: Duplicacion del eje de 220 kV” y la “repotenciación de la L/220 kV Caparacena-Gabias-Órgiva-Benahadux-Tabernas” han generado ya un enorme rechazo en la población de la Alpujarra y el Valle de Lecrín, ya que además de los enormes daños que conllevaría a nivel ambiental y socioeconómico, se considera que es una actuación totalmente innecesaria.


# Débiles argumentaciones

Se dan varios argumentos en defensa de estos proyectos que no se sostienen, sobre todo al ver la gran repercusión de la línea de Baza en la región:

* **Interconexión Sur-Levante:** REE argumenta en todas las actuaciones, menos en la de Baza, que la L/400 kV Caparacena-Hueneja-Tabernas-Litoral es la única interconexión Sur- Levante, y por lo tanto, en escenarios de alta importación desde Levante hacia el Sur, unido a una elevada producción de régimen especial y al fallo del eje de 400 kV, se produjeran sobrecargas máximas del 21% en la línea L/220 kV Caparacena-Gabias-Órgiva-Benahadux- Tabernas. Por algún motivo no se ha querido tener en cuenta que con la L/400 kV DC Baza- Caparacena, que sería la de mayor capacidad de transporte de las cuatro que vemos en el mapa, se reforzará enormemente la conexión Sur-Levante, de hecho estamos hablando de un incremento del 381,8% respecto de la capacidad de transporte actual, solo con la construcción de esta línea. Esta línea, junto con la que pasa por Órgiva, podría transportar perfectamente la energía de Almería a Granada en caso de fallo en la L/400 kV Caparacena- Hueneja-Tabernas-Litoral.
* **Evacuación de renovables:** Otro argumento que se da para aumentar la infraestructura eléctrica en la zona sur de Sierra Nevada es que servirá para evacuar la energía procedente de las renovables. Sin embargo, en el estudio realizado por la Consejería de Empleo, Empresa y Comercio para justificar la L/400 kV DC Baza-Caparacena encontramos un mapa sobre proyectos de generación eléctrica con tecnologías renovables en el que se analiza que suelos son favorables:

![](./images/dossier-image-4a.png)

Basándonos en este mapa podemos decir que los suelos de la región de la Alpujarra, el Valle de Lecrín y Sierra Lujar no son favorables para mas instalaciónes de energías renovables. Por lo tanto, carece de sentido argumentar que las líneas que por allí pasarán estarán destinadas a la evacuación de energías renovables. Según este estudio ( y debido a su topografia difícil) es una zona NO FAVORABLE para la instalacion de renovables. Este argumento tambien es falso.

* **Mayor fiabilidad y robustez del sistema de suministro:** Entendemos que este argumento debería sostenerse en algún tipo de carencia en el sistema eléctrico Andaluz o de la región y en tal caso podría ser justificable, pero observemos el Informe del Sistema Eléctrico 2017 realizado por la propia REE:

![](./images/dossier-image-4b.png)

Como podemos ver claramente en esta tabla, Andalucía está entre las cuatro únicas Comunidades Autónomas que tienen 0 energía no suministrada y un tiempo de interrupción medio de 0, lo que nos reafirma en decir que la red actual cumple con garantías su función y por lo tanto, las actuaciones no pueden basarse en estos falsos argumentos.

* **Reducción de los costes del sistema:** En Enero de 2018, el delegado de Gobierno en Andalucía, Antonio Sanz, anunció que estas actuaciones costarían un total de 336 millones de euros, y aunque a priori pueda parecer contradictorio, REE argumenta que esto reducirá los costes del sistema eléctrico. Entendemos que este argumento se basa en que la resolución de las restricciones técnicas del sistema, como son las sobrecargas, favorecen el correcto funcionamiento del sistema y con ello reducen los costes de mantenimiento. No se puede hablar de reducción de costes del sistema cuando no se está siendo nada conservador con las proyecciones. Por un lado, recuperar la inversión para REE está garantizado a través de las facturas de la luz que pagan los consumidores. Por otro lado, no se tienen en cuenta los costes sociales, económicos y ambientales en dos comarcas con tanto valor para la provincia de Granada. Creemos que el primer paso para reducir costes de infraestructuras innecesarias seria que un órgano imparcial auditara y analizara las necesidades reales del sistema. Las puertas giratorias han dejado bien claro que el Gobierno no puede ser ese órgano.

# La “transparencia” de REE

Además de todo lo mencionado hasta ahora, nos encontramos con que REE no es nada transparente en sus planificaciones, oculta sus verdaderas intenciones, utiliza cualquier vacío legal a favor de sus intereses y para nada se centra en mejorar el sistema eléctrico en función de las necesidades reales de la ciudadanía. Un ejemplo de esto lo tenemos en la SE de Saleres, en las líneas de E/S a la línea Gábias-Orgiva-Berja, que están diseñadas con doble circuito de 862 MVA/circuito y se conectan a una línea de SC de una capacidad de transporte de 439 MVA (después de la repotenciación). En ningún lugar se justifica la conexión entre unas líneas con capacidades de transporte tan dispares (cuadruplican la existente). La única explicación que se nos ocurre es que en la próxima planificación quieran justificar la necesidad de aumentar la capacidad de transporte de la línea existente de nuevo. Esto conlleva un despilfarro de dinero y una abuso sobre el medio natural inaceptable.
Otro ejemplo es que varias líneas proyectadas en todo el territorio español, entre ellas la línea Benahadux-Saleres, que iría por Sierra Lújar, se diseñan aisladas a 400 kV pero se tramitan con una tensión inicial de 220kV. Las competencias de autorización de una instalación de 220kV dependen de la Junta de Andalucía, en cambio para una de 400kV habría que ir al gobierno central. De este modo logran construir una línea de 400 kV, que al inicio trabajará a 220 kV, pero que el gobierno central se verá obligado a tramitar a 400 kV una línea que ya está construida.

# Anexo: Características técnicas de las actuaciones

![L/400 kV DC Baza-Caparacena](./images/dossier-anexo-a.png)
![TS-1: Duplicación del eje de 220 kV](./images/dossier-anexo-b.png)
![L/220 kV DC Benahadux-Saleres](./images/dossier-anexo-c.png)

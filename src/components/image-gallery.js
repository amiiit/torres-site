import React, { useCallback } from "react";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import styled from '@emotion/styled'

function ImageGallery({ photos, className, direction }) {
  const [currentImage, setCurrentImage] = React.useState(0);
  const [viewerIsOpen, setViewerIsOpen] = React.useState(false);

  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const galleryPhotos = photos.map(photo => ({
      src: photo.fixed.src,
      x: 400,
      width: photo.fixed.width,
      height: photo.fixed.height
  }))

  const views = photos.map(photo => {
      return {
        src: photo.fluid.src,
      }
  })

  return (
    <Root className={className}>
      <Gallery photos={galleryPhotos} onClick={openLightbox} direction={direction ? direction : 'row'}/>
      <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              views={views}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </Root>
  );
}

const Root = styled.div`

`

export default ImageGallery

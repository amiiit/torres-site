import * as React from "react"
import styled from "@emotion/styled"
import { Link } from "gatsby"
import { NewsNode } from "../../pages/blog"
import { format } from "date-fns"

interface Props {
  newsNode: NewsNode
  className?: string
  lang: string
}

const NewsItem = (props: Props) => {
  const newsItemURLPart = props.newsNode.slug
    ? props.newsNode.slug
    : props.newsNode.id
  return (
    <Root className={props.className}>
      <Title
        dangerouslySetInnerHTML={{ __html: props.newsNode.title }}
        to={`/${newsItemURLPart}`}
      />
      <Excerpt dangerouslySetInnerHTML={{ __html: props.newsNode.excerpt }} />
      <Footer>
        {
          props.newsNode.date ? <ItemDate>{format(new Date(props.newsNode.date), 'dd-MM-yyyy')}</ItemDate> : null
        }
      <Link
        to={`/${newsItemURLPart}`}
        css={{
          color: "red !important",
          textDecoration: "none",
          textAlign: "right",
        }}
      >
        {
          {
            en: "Read More",
            es: "Seguir leyendo",
          }[props.lang]
        }
      </Link>
      </Footer>

    </Root>
  )
}

const Root = styled.div`
  background: white;
  border-radius: 8px;
  box-sizing: border-box;
  border: 1px solid #e0e0e0;
  transition: box-shadow linear 0.218s;
  padding: 14px;
  overflow: hidden;
  &:hover {
    box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.302),
      0 1px 3px 1px rgba(60, 64, 67, 0.149);
  }
  display: flex;
  flex-direction: column;
`

const Title = styled(Link)`
  font-size: 28px;
  font-weight: bold;
  text-decoration: none;
`

const Footer = styled.div`
  display: flex;
  flex-direction: row;
  > :first-child {
    flex: 1;
  }
`

const ItemDate = styled.div``

const Excerpt = styled.div``
export default NewsItem

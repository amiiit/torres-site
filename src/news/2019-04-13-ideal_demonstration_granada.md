---
date: "2019-03-07"
title: "Demonstration in Granada - Ideal"
type: "news"
lang: "en"
---
We have demonstrated in Granada against the plan of REE. Read about it in the following link 
[on the website of the newspaper Idea about our demonstration in Granada](https://www.ideal.es/granada/provincia-granada/cientos-personas-salen-20190413162118-nt.html)

---
date: "2019-03-08"
lang: "en"
path-es: "2019-03-08_orgiva_aprueba_alegaciones"
type: "news"
title: "Órgiva Approves Allegations"
---
Órgiva's town hall has accepted the allegations. You can read more about it in the [newspaper Ideal (In Spanish)](https://www.ideal.es/granada/provincia-granada/orgiva-aprueba-alegaciones-20190308141422-nt.html)

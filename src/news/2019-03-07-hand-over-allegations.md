---
date: "2019-03-07"
title: "Hand over allegations to the delagation of Granada"
type: "news"
lang: "en"
---
The civil platforms of the Alpujarra en the Valle de Lecrín handed in more than 4000 allegations to the delagacion of Granada

[Click here to see the interview on YouTube](https://www.youtube.com/watch?v=Jo5cihU6H8A)

[![](../images/2019-03-07-entregar-alegaciones.png)](https://www.youtube.com/watch?v=Jo5cihU6H8A)

**Note August 14th 2019** - The allegations still remain without a response

---
date: "2019-04-14"
lang: "en"
type: "news"
title: "We made it to the front page of Ideal"
---
The local newspaper Ideal of Granada wrote about our demonstration in Granada in the front page
![image](../images/ideal-newpaper.png)

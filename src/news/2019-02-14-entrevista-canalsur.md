---
date: "2019-02-14"
title: "Entrevista em Canal Sur"
type: "news"
lang: "es"
---
Concentración de la Plataforma Ciudadana en Plaza Alpujarra. Convocatoria a la ciudadanía para firmar las Alegaciones.

[Verlo en YouTube](https://www.youtube.com/watch?v=IP0bNB4T9QE)

---
date: "2019-03-08"
lang: "es"
path-en: "2019-03-08_orgiva_approves_allegations"
type: "news"
title: "Órgiva aprueba las alegaciones"
---
El ayuntamiento de Órgiva aprobó las alegaciones, pueden lees más en [el diario Ideal](https://www.ideal.es/granada/provincia-granada/orgiva-aprueba-alegaciones-20190308141422-nt.html)

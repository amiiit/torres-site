---
date: "2019-02-14"
title: "Canal Sur"
type: "news"
lang: "en"
---
Canal Sur was covering the assembly in the Plaza Alpujarra in Órgiva

[Click here to see the video on the website of Canal Sur](http://www.canalsur.es/noticias/andalucia/granada/orgiva-protesta-contra-las-torres-de-alta-tension/1395140.html)

[![](../images/canalsur-video.png)](http://www.canalsur.es/noticias/andalucia/granada/orgiva-protesta-contra-las-torres-de-alta-tension/1395140.html)

import SEO from "../components/seo"
import Layout from "../components/layout"
import React from "react"
import styled from "@emotion/styled"

const NEWS_PATH = {
  en: "/news",
  es: "/noticias",
}

const NewsPage = ({ pageContext }) => {
  const { data, lang } = pageContext
  return (
    <Layout lang={lang} pathEn={NEWS_PATH.en} pathEs={NEWS_PATH.es} currentPath={NEWS_PATH[lang]}>
      <SEO title="Noticias"/>

      <h1 css={{marginBottom: 24}}>{{es: "Noticias", en: "News"}[lang]}</h1>

      {
        data && data.filter(node => node.frontmatter.lang === lang)
          .sort((n1, n2) => {
            return n1.frontmatter.date < n2.frontmatter.date
          })
          .map(node => (
            <NewsItemRoot key={node.frontmatter.title}>
              <Header>
                <Date>
                  {node.frontmatter.date}
                </Date>
                <Title>
                  {node.frontmatter.title}
                </Title>
              </Header>
              <div dangerouslySetInnerHTML={{ __html: node.html }}/>
            </NewsItemRoot>
          ))
      }
    </Layout>
  )

}
const NewsItemRoot = styled.div`
  margin-bottom: 48px;
  
`

const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
`
const Date = styled.div`
  line-height: 1;
  margin-right: 8px;
  font-size: 14px;
`
const Title = styled.div`
  font-size: 18px;
  font-weight: bold;
`
export default NewsPage

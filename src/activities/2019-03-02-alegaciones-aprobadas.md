---
type: "activity"
date: "2019-03-02"
title: "Alegaciónes aprobadas"
lang: "es"
videos: 
  - url: "https://www.youtube.com/watch?v=Jo5cihU6H8A"
    title: "Picha Aquí para ver en YouTube"
    coverImage: "../images/2019-03-01-alegaciones-video-thumb.png"
---
Las dos plataformas de la Alpujarra y el Valle de Lecrín entregan las alegaciónes contra el proyecto de REE

**¡Las alegaciónes siguen más de 200 días sin respuesta!**

[Leer el artículo en la pagina del priódico IDEAL](https://www.ideal.es/granada/provincia-granada/orgiva-aprueba-alegaciones-20190308141422-nt.html)

---
type: "activity"
date: "2019-03-01"
title: "IES Pitres - Trabajo de alumnas"
lang: "en"
videos: 
  - url: "/2019-03-01-ies-pitres-video-alumnas.mp4"
    title: "Ver el video de las alumnas y alumnos"
    coverImage: "../images/2019-03-01-ies-pitres.png"
---
[Pincha aquí para leer el trabajo que hicieron las alumnas y los alumnos del IES Pitres (PDF)](/2019-03-01-ies-pitres-campana_alta_tension_informacion.pdf)
[![](../images/2019-03-01-ies-pitres-trabajo-cover.png)](/2019-03-01-ies-pitres-campana_alta_tension_informacion.pdf)

<br/>

[Pincha aquí para leer la entrevista con el profesor Manolo (PDF)](/2019-03-01-ies-pitres-entrevista-manolo.pdf)
[![](../images/2019-03-01-ies-pitres-entrevista-cover.png)](/2019-03-01-ies-pitres-entrevista-manolo.pdf)

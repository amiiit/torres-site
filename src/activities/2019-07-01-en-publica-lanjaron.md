---
type: "activity"
date: "2019-07-01"
title: "Publica Lanjarón"
lang: "en"
imageGallery: "2019-07-publica-lanjaron"
videos:
  - url: "https://www.youtube.com/watch?v=Wv26As0nVQo"
    title: "Ver el video en YouTube"
    coverImage: "../images/2019-07-publica-lanjaron/2019-07-Lanjaron-1.jpeg"
---
La plataforma "Di No A Las Torres de alta tensión" participa en la publica de Lanjarón
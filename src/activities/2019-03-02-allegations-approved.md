---
type: "activity"
date: "2019-03-02"
title: "Allegations approved"
lang: "en"
videos: 
  - url: "https://www.youtube.com/watch?v=Jo5cihU6H8A"
    title: "Click to watch on YouTube"
    coverImage: "../images/2019-03-01-alegaciones-video-thumb.png"
---
The two platforms of the Alpujarra and Valle the Lecrín hand over the allegations

**The allegation still didn't get any response for more than 200 days!**

[Read the article (Spanish) in the news paper IDEAL](https://www.ideal.es/granada/provincia-granada/orgiva-aprueba-alegaciones-20190308141422-nt.html)

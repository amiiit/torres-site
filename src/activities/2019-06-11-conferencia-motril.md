---
type: "activity"
date: "2019-06-11"
title: "Conferencia en Motril"
lang: "es"
videos:
  - url: "https://www.youtube.com/watch?v=wpICHbpPi5c"
    title: "Ver la entrevista en YouTube"
    coverImage: "../images/2019-06-11-entrevista-conferencia-motril.png"
---
![](../images/conferencia-motril.png)

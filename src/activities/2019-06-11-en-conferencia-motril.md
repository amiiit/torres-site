---
type: "activity"
date: "2019-06-11"
title: "Conference in Motril"
lang: "en"
videos:
  - url: "https://www.youtube.com/watch?v=wpICHbpPi5c"
    title: "Watch the interview on YouTube"
    coverImage: "../images/2019-06-11-entrevista-conferencia-motril.png"
---
![](../images/conferencia-motril.png)

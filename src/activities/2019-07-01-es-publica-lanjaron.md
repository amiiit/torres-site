---
type: "activity"
date: "2019-07-01"
title: "Publica Lanjarón"
lang: "es"
imageGallery: "2019-07-publica-lanjaron"
videos: 
  - title: "Ver el video en YouTube"
    url: "https://www.youtube.com/watch?v=Wv26As0nVQo"
    coverImage: "../images/2019-07-publica-lanjaron/2019-07-Lanjaron-1.jpeg"
---
La plataforma "Di No A Las Torres de alta tensión" participa en la publica de Lanjarón
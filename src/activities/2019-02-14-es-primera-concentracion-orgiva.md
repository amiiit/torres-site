---
type: "activity"
date: "2019-02-14"
imageGallery: "2019-02-14-primera-concentracion-orgiva"
lang: "es"
title: "Primera Concentración en Órgiva"
videos:
    - title: "Pincha aquí para ver el video en YouTube"
      url: "https://www.youtube.com/watch?v=CzZ4tuGxlMQ"
      coverImage: "../images/2019-02-14-manifestacion-plaza.png"
    - title: "Ver la reportage de Canal Sur"
      url: "http://www.canalsur.es/noticias/andalucia/granada/orgiva-protesta-contra-las-torres-de-alta-tension/1395140.html"
      coverImage: "../images/canalsur-video.png"
    - title: "Entrevista de Canal Sur"
      url: "https://youtu.be/IP0bNB4T9QE?t=20"
      coverImage: "../images/2019-02-14_Entrevista_Canal_Sur.png"
---

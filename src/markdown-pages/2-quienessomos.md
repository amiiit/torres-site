---
path: "/quienes-somos"
title: "Quiénes Somos"
lang: "es"
path-en: "/who-we-are"
order: 2
type: "page"
---
# Quiénes Somos

La plataforma ciudadana “Di No A Las Torres” se ha formado para protestar en contra de un nuevo proyecto de REE que pretende llenar la Alpujarra de torres de alta tensión, afectando la salud, la economía, el paisaje, el medio ambiente y la biodiversidad de nuestra comarca. Nosotros estamos en contacto con la plataforma “Di No A Las Torres en el Valle de Lecrín“ para defender nuestros intereses.

Se han presentado más de 4000 alegaciones todavía no contestadas, dónde se expone con claridad los puntos por los que estamos en contra de ésta autovía de electricidad.

Esta construcción afectaría indudablemente a nuestras acequias, sistema de riego histórico con el que mantenemos verde nuestro entorno, nuestros olivos y naranjos. En consecuencia dañaría nuestra agricultura y turismo, las dos grandes fuentes de nuestra economía. Esta plataforma está apoyada por empresas, asociaciones y ayuntamientos, que como todos, ven el peligro futuro y dañino de este proyecto.
<br/>
<br/>
<br/>
<br/>

![nosotros](../images/quienessomos1.jpg)

<br/>
<br/>

<span style="text-align:center">
<h1>Manifiesto</h1>
<h2>No a Las Torres de Alta Tensión</h2>
</span>
<br />
Las plataformas de La Alpujarra y el Valle de Lecrín, manifestamos nuestro rechazo al proyecto de REE que es una agresión a nuestro paisaje, nuestra economía y nuestra salud.

* **1º**  El impacto paisajístico y el destrozo medio ambiental es muy grande, tanto en la ladera donde, según el proyecto van ubicadas las torres, o la ladera norte de Sierra de Lújar. Las enormes dimensiones de estas torres, obliga para su colocación a hacer numerosos carriles, plataformas, tala de árboles…en la ladera sur de Sierra Nevada, entre Cáñar y Órgiva, una zona con acuíferos, varias acequias de riego, dos ríos y arboleda. Zona de muy variadas especies de aves. Destrozan todo el corredor verde del Valle de Lecrín. Además de un gran impacto visual desde cualquier punto de las vegas de estas dos comarcas.

* **2º** Es una ruina para los afectados directos al expropiarles sus propiedades y atravesar sus casas con torres de A.T. En el procedimiento que siguen las líneas  de A.T. dice: No podrá imponerse servidumbre de paso para las líneas de alta tensión: Sobre edificios, sus patios, sus corrales, centros escolares, campos deportivos, y jardines y huertos, también cerrados, anejos a viviendas que ya existan al tiempo de decretarse la servidumbre…”

* **3º** Es acabar con la economía del Valle de Lecrín y La Alpujarra por donde transcurren las líneas. Desde 1991 a través de los programas LEADER, financiados por la Comunidad Europea, se desarrolló el plan de turismo rural. Se animó a los habitantes de estas comarcas a optar por este tipo de turismo y así complementar las bajas rentas obtenidas con la actividad agraria. Esta economía se ha consolidado en estos años, la población se ha mantenido y ha proliferado otro tipo de negocios, especialmente comercios, restauración, artesanía y servicios.  La actividad turística se basa fundamentalmente en el paisaje, que siendo un valor en si mismo, es además el motor de nuestra economía.

* **4º**  Y muy importante, la salud.  Estadísticamente, en aquellos lugares, donde las líneas de A.T. están cerca de la población, proliferan diversos tipos de enfermedades, especialmente el cáncer y leucemia infantil.  Al tratarse de una población dispersa, las líneas pasan por encima de bastantes cortijos.


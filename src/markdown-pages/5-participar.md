---
title: "Como Participar"
path: "/participar"
lang: "es"
path-en: "/participate"
order: 5
type: "page"
---
# Hay muchas maneras de participar

Lo mas importante es acudir a las concentraciones y manifestaciones para hacer visible cuant@s somos l@s que queremos proteger nuestra comarca.

![](../images/textseparatorsmall.png)

Puedes mantenerte al día en [Facebook](https://www.facebook.com/alpujarranoalastorres/). Allí podrás encontrar las noticias más actuales y te informaremos de las actividades que quizás necesiten de tu apoyo.:

[![alt="Síguenos en facebook"](../images/siguenos-facebook.png)](https://www.facebook.com/alpujarranoalastorres/)

![](../images/textseparatorsmall.png)

Puedes hacer una donación para cubrir gastos (pancartas, carteles, abogados...) al siguiente numero de cuenta:

**Titular:** Asociación eco-logico<br/>
**IBAN:** ES94 3023 0066 4465 5489 2205<br/>
**BIC:** BCOEESMM023

![](../images/textseparatorsmall.png)

Y si quieres involucrarte más estás bienvenid@ a nuestras asambleas:

**cada segundo martes a las 20h en el ayuntamiento de Órgiva.**

![](../images/textseparatorsmall.png)

##Contacto

Si tienes cualquiér pregunta ó duda mándanos un correo electronico: **[alpujarrasintorres@gmail.com](mailto:alpujarrasintorres@gmail.com)** 

![](../images/textseparatorsmall.png)


<h1 style="text-align:center">
DI NO A LAS TORRES DE ALTA TENSIÓN
</h1>

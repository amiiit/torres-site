---
path: "/start"
title: "Start"
lang: "en"
path-es: "/"
order: 1
type: "page"
---

# Our Alpujarra is in danger!

The Alpujarra, located in the south of Spain, has one of the most beautiful mountainous landscapes in the country and a great diversity of ecosystems due its special geological and climatic qualities.  Much of the Alpujarra is in the protected zone of the Sierra Nevada Natural Park, the Sierra Nevada being the highest mountain chain on the Iberian Peninsula.

<recent-posts></recent-posts>

The Alpujarra is known as the land between the sun and the sea, between the Costa Tropical and the Sierra Nevada.

![plan](../images/cartel-tenecesita.png) 

It is a district of huge cultural and touristic interest, due to its delightful countryside and its great historical heritage.  You can still find in its villages Arab influenced structures and architecture.  Even the extensive irrigation systems from that time remain which maintain its great landscape and unique ecology.

And this unique landscape with its spectacular mountainous geology is in danger because REE wants to build a high tension line from the Lecrin Valley throughout the length of the Alpujarras.  The pylons of up to 80 metres and required construction work will threaten the nature and ecology of this unique and special valley.

The Alpujarra according REE plans

**SAY NO TO THE HIGH TENSION PYLONS**

**DI NO A LAS TORRES DE ALTA TENSIÓN**

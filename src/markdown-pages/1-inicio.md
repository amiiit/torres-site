---
path: "/"
title: "Inicio"
lang: "es"
path-en: "/start"
order: 1
type: "page"
---
# ¡Nuestra Alpujarra está en peligro!
La Alpujarra, ubicada en el sur de España, pertenece a uno de los paisajes montañosos más bellos de este país y cuenta con una gran diversidad de ecosistemas gracias a las características especiales de su geología y climatología. Gran parte de la Alpujarra se encuentra incluida en la zona protegida “Parque natural de Sierra Nevada”, siendo la Sierra Nevada la cadena montañosa mas alta de la península ibérica.

<recent-posts></recent-posts>

La Alpujarra esta conocida como la zona entre “el Sol y la Nieve”, entre “la Costa Tropical y la Sierra Nevada“.

Es una comarca de inmenso interés cultural y turístico por su paisaje encantador y su gran pasado histórico. En sus pueblos todavía se encuentran estructuras y arquitecturas de influencia árabe, y de aquel época aún se conserva un extenso sistema de acequias para el regadío, que mantiene el gran paisaje y su ecosistema único.

<br />

![plan](../images/cartel-tenecesita.png) 

<br />

Y este paisaje único con su espectacular orografía está en peligro porque REE quiere construir su red de alta tensión desde el Valle de Lecrín a lo largo de toda la Alpujarra. Las torres, que pueden llegar a medir 80 metros , y los trabajos de construcción necesarios pondrían en peligro la naturaleza y el ecosistema de esta zona tan única y especial, como lo es la Alpujarra.

<br />

![plan](../images/vista-con-torres.jpg "La Alpujarra según los planes de REE") 

<br />
<h1 style="text-align:center;">¡DI NO A LAS TORRES DE ALTA TENSIÓN!</h1>
<br />
<br />

<br />

![torre](../images/textseparator.png)

<br />


Para tener una idea de las dimensiones y entender el impacto visual:

![La Alpujarra según los planes de REE](../images/esquemaalpujarra.png)

<br />

![torre](../images/textseparator.png)

<br />


### Este video explicativo está hecho por miembros de la plataforma de la Alpujarra:
&nbsp;<video-player title="Pincha aquí para ver el video en YouTube" url="https://www.youtube.com/watch?v=HPJfy9tJFn0">
![](../images/explanatory-video-thumb.png)
</video-player>

<br/>

#[¿Quieres ayudar? ¡Haz un click aquí!](/participar/)

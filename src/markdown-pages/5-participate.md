---
title: "How to help"
path: "/participate"
lang: "en"
path-es: "/participar"
order: 5
type: "page"
---
# There are lots of ways to participate:

The most important thing is to get along to the demonstrations  to show how many of us want to protect our region.

![](../images/textseparatorsmall.png)

You can keep up to date on [Facebook](https://www.facebook.com/alpujarranoalastorres/). There you´ll find all the latest news and we´ll let you know about activities in which we might need your help.

[![alt="Follow us on facebook"](../images/siguenos-facebook-en.png)](https://www.facebook.com/alpujarranoalastorres/)

![](../images/textseparatorsmall.png)

You can make a donation to help meet our costs (banners, posters, lawyers…) to the following account number:

**Titular:** Asociación eco-logico<br/>
**IBAN:** ES94 3023 0066 4465 5489 2205<br/>
**BIC:** BCOEESMM023

![](../images/textseparatorsmall.png)

And if you want to get more involved you are welcome at our meetings:

**Every other Tuesday at 8pm in the town hall.**

![](../images/textseparatorsmall.png)

##Contact

For any other question, just send us an email: **[alpujarrasintorres@gmail.com](mailto:alpujarrasintorres@gmail.com)** 

![](../images/textseparatorsmall.png)

<h1 style="text-align:center">
SAY NO TO HIGH VOLTAGE TOWERS!
 </h1>

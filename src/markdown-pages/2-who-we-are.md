---
path: "/who-we-are"
title: "About us"
lang: "en"
path-es: "/quienes-somos"
order: 2
type: "page"
---
# About us

The citizens’ action group “Di No A Las Torres” has been formed to protest against REE trying to fill the Alpujarras with high tension pylons, affecting the health, economy, countryside, environment and biodiversity of our district.

We are working with the Lecrin Valley “Di No A Las Torres” action group to defend our shared interests. Together we have presented over 4000 objections, still unanswered, where we clearly set out the reasons why we are against this electrical highway.

![nosotros](../images/quienessomos1.jpg)

This construction will undoubtedly affect our historic irrigation system that keeps everything green and sustains the olive and orange trees.  As a result, this will damage agriculture and tourism which are the mainstays of our economy.

Our action group is supported by businesses, social organisations and town councils that like everybody sees the dangerous future and damage that will be done by this project.
